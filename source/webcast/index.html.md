---
layout: markdown_page
title: "Webcasts"
---

## On this page
{:.no_toc}

- TOC
{:toc}  


## Upcoming Webcasts  

### GitLab for Enterprises
This is a reoccuring demo webcast that is held every two weeks on Wednesday morning. You'll get a first-hand look at how GitLab integrates version control, project management, code review, testing, and deployment to help enterprise teams move faster from planning to monitoring.   

We are currently changing the layout of this webcast series. There are no future dates scheduled at this time. Please check back in the near future.


### Starting and Scaling DevOps with Gary Gruver   
**June 19th** - 9am PT / 4pm UTC  
[Register today](/webcast/starting-scaling-devops/) and join Gary Gruver, author of Starting and Scaling DevOps in the Enterprise, as he covers how to analyze your current deployment pipeline to target your first improvements on the largest inefficiencies in software development and deployment.


### Release Radar: Auto DevOps   
**June 27th** - 9am PT / 4pm UTC  
[Join us](/webcast/auto-devops) for a live broadcast to learn how Auto DevOps simplifies your deployment pipeline to accelerate delivery by 200%, improves responsiveness, and closes the feedback gap between you and your users.

### DevOps Discussion with Gary Gruver
**August 2nd** - 9am PT / 4pm UTC
[Register today](/webcast/gary-gruver-discussion/) and join Gary Gruver, author of Starting and Scaling DevOps in the Enterprise, as he shares his experience consulting with organizations to successfully adopt a DevOps model. He’ll offer advice on the influencers who should be involved in discussions and pinpoint the areas were a transformation should begin.


## Past Webcasts   

### Overcoming Barriers to DevOps Automation - a 4-part series
**Session 1**: Let's Talk DevOps and Drawbacks   
**Session 2**: Solving the Collaboration Conundrum: How to Make the DevOps Dream a Reality    
**Session 3**: Removing Barriers Between Dev and Ops: Setting Up a CI/CD Pipeline   
**Session 4**: Getting Started with CI/CD on GitLab   

[Watch](/webcast/devops-automation/) the webcast recordings.
